/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : payment

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-07-26 18:57:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pay_bill_paypal
-- ----------------------------
DROP TABLE IF EXISTS `pay_bill_paypal`;
CREATE TABLE `pay_bill_paypal` (
  `bill_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `agreement_id` char(50) NOT NULL COMMENT '协议ID',
  `user_id` int(11) NOT NULL COMMENT '账号ID',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '使用状态1正常 -1失效',
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`bill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='paypal协议支付授权ID记录';

-- ----------------------------
-- Table structure for pay_bill_stripe
-- ----------------------------
DROP TABLE IF EXISTS `pay_bill_stripe`;
CREATE TABLE `pay_bill_stripe` (
  `bill_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `customer_id` char(50) NOT NULL COMMENT '协议ID',
  `user_id` int(11) NOT NULL COMMENT '用户标识',
  `status` tinyint(1) DEFAULT '1' COMMENT '使用状态 1正常 -1失效',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`bill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='stripe协议支付 授权ID记录';

-- ----------------------------
-- Table structure for pay_trade_ppa
-- ----------------------------
DROP TABLE IF EXISTS `pay_trade_ppa`;
CREATE TABLE `pay_trade_ppa` (
  `trade_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '交易ID',
  `bill_id` int(11) DEFAULT NULL COMMENT '账单协议ID',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `goods_title` varchar(200) DEFAULT NULL COMMENT '商品标题',
  `goods_desc` varchar(200) DEFAULT NULL COMMENT '商品描述',
  `goods_amount` decimal(10,2) DEFAULT NULL COMMENT '商品金额',
  `goods_unit` char(10) DEFAULT NULL COMMENT '费用单位，USD RMB',
  `request_ip` char(20) DEFAULT NULL COMMENT '请求IP',
  `request_param` varchar(255) DEFAULT NULL COMMENT '原始参数',
  `status` tinyint(1) DEFAULT '-1' COMMENT '交易状态 -1未支付 1已支付',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`trade_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='paypal协议支付交易记录';

-- ----------------------------
-- Table structure for pay_trade_pps
-- ----------------------------
DROP TABLE IF EXISTS `pay_trade_pps`;
CREATE TABLE `pay_trade_pps` (
  `trade_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '交易ID',
  `goods_title` varchar(200) DEFAULT NULL COMMENT '商品标题',
  `goods_desc` varchar(200) DEFAULT NULL COMMENT '商品描述',
  `goods_amount` decimal(10,2) DEFAULT NULL COMMENT '商品金额',
  `goods_unit` char(10) DEFAULT NULL COMMENT '费用单位，USD RMB',
  `request_ip` char(20) DEFAULT NULL COMMENT '请求IP',
  `request_param` varchar(255) DEFAULT NULL COMMENT '原始参数',
  `status` tinyint(1) DEFAULT '-1' COMMENT '交易状态 -1未支付 1已支付',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`trade_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COMMENT='paypal常规支付交易记录';

-- ----------------------------
-- Table structure for pay_trade_stripe
-- ----------------------------
DROP TABLE IF EXISTS `pay_trade_stripe`;
CREATE TABLE `pay_trade_stripe` (
  `trade_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '交易ID',
  `bill_id` int(11) DEFAULT NULL COMMENT '交易信息ID',
  `user_id` int(11) DEFAULT NULL COMMENT '用户标识ID',
  `goods_title` varchar(200) DEFAULT NULL COMMENT '商品标题',
  `goods_desc` varchar(200) DEFAULT NULL COMMENT '商品描述',
  `goods_amount` decimal(10,2) DEFAULT NULL COMMENT '商品金额',
  `goods_unit` char(10) DEFAULT NULL COMMENT '费用单位，USD RMB',
  `request_ip` char(20) DEFAULT NULL COMMENT '请求IP',
  `request_param` varchar(255) DEFAULT NULL COMMENT '原始参数',
  `status` tinyint(1) DEFAULT '-1' COMMENT '交易状态 -1未支付 1已支付',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`trade_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='stripe协议支付交易记录';
