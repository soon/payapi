<?php
namespace app\api\logic;

use think\Db;
use think\Controller;
use app\api\model\TradePps;
use app\api\service\Paypalsmart;

/**
 * Paypal 常规支付逻辑层
 * @author sam
 * Date 2019/07/18
 */
class SmartLogic extends Controller
{
    // 创建订单
    public function createOrder($tradeId)
    {
        $data = $this->getInfo($tradeId);

        // 支付信息
        $info = $this->payInfo($data);

        // 创建订单
        $pay = new Paypalsmart();
        $res = $pay->createOrder($info);
        return $res;
    }

    // 支付信息
    public function payInfo($data)
    {
        return [
            "intent" => "CAPTURE",
            "purchase_units" => [[
                "reference_id" => "test_ref_id1",
                "description"  => $data['goods_desc'],
                "amount" => [
                    "value" => $data['goods_amount'],
                    "currency_code" => $data['goods_unit'],
                ]
            ]],
            "redirect_urls" => [
                "cancel_url" => "https://example.com/cancel",
                "return_url" => "https://example.com/return"
            ]
        ];
    }

    // 支付订单
    public function payOrder($tradeId, $orderId)
    {
        // 支付
        $pay = new Paypalsmart();
        $res = $pay->captureOrder($orderId);
        // 支付完成
        if (strtolower($res->result->status) == "completed") {
            $this->saveTrade($tradeId);
        }

        return $res;
    }

    // 获取订单
    public function verfyOrder($orderId)
    {
        // 获取
        $pay = new Paypalsmart();
        $res = $pay->getOrder($orderId);
        return $res;
    }

    // 保存请求信息
    public function saveRequest($data)
    {
        $list = [
            'goods_title'  => $data['goods_title'],
            'goods_desc'   => $data['goods_desc'],
            'goods_amount' => $data['goods_amount'],
            'goods_unit'   => $data['goods_unit'],
        ];

        // 请求参数
        $list['request_ip']    = getClientIp();
        $list['request_param'] = serialize($data);
        $list['create_time']   = date("y-m-d H:i:s", time());

        // 保存数据库
        $insertId = Db::name('trade_pps')->insertGetId($list);
        return $insertId;
    }


    // 获取交易记录信息
    public function getInfo($id)
    {
        return TradePps::get($id);
    }

    // 更新交易记录表
    public function saveTrade($trade_id)
    {
        $trade = new TradePps;
        $res   = $trade::where('trade_id', $trade_id)->update(['status' => 1]);
        return $res;
    }

    /**
     * 接口输出
     *
     * @param      intval   $code     错误代码
     * @param      boolean  $status   请求结果
     * @param      array    $data     参数
     * @param      string   $apiCode  接口返回代码
     * @param      string   $apiMsg   接口返回信息
     *
     * @return     array    ( response )
     */
    public function returnMsg($code, $status = false, $data = [], $apiCode = '', $apiMsg = '')
    {
        $msg  = config("logic.paypal_agreement_msg");
        $data = [
            'result'   => empty($status)? "fail": "success",
            'code'     => $code,
            'message'  => $msg[$code],
            'api_code' => $apiCode,
            'api_msg'  => $apiMsg,
            'data'     => $data,
        ];

        return $data;
    }


}
