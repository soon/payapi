<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 接口配置
// +----------------------------------------------------------------------

return [

	// 接口配置
	// paypal协议支付
	'paypal_merchant' => [
        'merchant_name' => 'sanwcom_api1.163.com',
        'merchant_pass' => 'XR7B9DSTNXKHMYX2',
        'merchant_sign' => 'ADgDKoXYoItrppOSy.o-I5Gnw-h1A5nNMVyf7MdfaOdmsnq5d5O.LAyQ',
	],
	// paypal常规支付
	'paypal_app' => [
        'app_clientid' => 'ASG1SX6QsAygMtUto8pTCGEhD4GCn9Bprc1x9c09ijG5yiht3x-AxbbLDj6plT4p0ud-TXDpe7ddjadW',
        'app_secret'   => 'EKBe4J6m4Q2u9NuvPDuloUfpy6Jjl0zwWbCMC82OH5iS1JJijQRAOz32n4Op-VsDswyl441L9li-wXAf',
	],
	// stripe协议支付
	'stripe_config' => [
        'publish_key'  => 'pk_test_pBpGhM6ep2AtmRZBKjfj9XfI00VSKv80DW',
        'secret_key'   => 'sk_test_3xVzyuMVrRuFSE2bOvzAEFUL00jFLsemHY',
	],

	// 消息响应
	// 创建支付
	'return_msg' => [
		102 => '请求方式错误',
		103 => '请选择支付方式',
		104 => '参数缺失',
		105 => '参数格式错误',
		106 => '支付创建失败',
		199 => '服务器错误',
		200 => '创建成功',
	],
	// paypal协议支付
	'paypal_agreement_msg' => [
		102 => '请求方式错误',
		103 => '请求参数缺失',
		104 => '支付参数缺失',
		105 => '数据储存失败',
		106 => '授权取消',
		107 => '授权失败',
		108 => '支付失败',
		109 => '授权支付失败',
		199 => '服务器连接失败',
		200 => '支付成功',
	],
	// stripe协议支付
	'stripe_msg' => [
		102 => '请求方式错误',
		103 => '请求参数缺失',
		104 => '数据储存失败',
		105 => '银行卡注册失败',
		106 => '客户注册失败',
		107 => '支付发起失败',
		199 => '服务器连接失败',
		200 => '支付成功',
	],
];
