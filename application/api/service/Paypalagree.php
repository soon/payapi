<?php
namespace app\api\service;

use think\Controller;
use think\facade\Env;

/**
 * Paypal 通过协议支付
 * @author sam
 * Date 2019/07/10
 */
class Paypalagree extends Controller
{

    // 授权请求
    private $agragreementUrl = "https://api-3t.sandbox.paypal.com/nvp"; //协议请求地址
    // 授权回调地址
    private $excuteUrl       = 'https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=';

    // 商家信息
    private $merchantName = '';
    private $merchantPass = '';
    private $merchantSign = '';

    public function __construct()
    {
        parent::__construct();

        $config  = config("logic.paypal_merchant");

        $this->merchantName = $config['merchant_name'];
        $this->merchantPass = $config['merchant_pass'];
        $this->merchantSign = $config['merchant_sign'];
    }

    // 购买时设置结算协议
    public function requestAgreement($amount, $unit, $returnUrl, $cancelUrl)
    {
        // 请求数据
        $data = [
            'USER'                           => $this->merchantName,
            'PWD'                            => $this->merchantPass,
            'SIGNATURE'                      => $this->merchantSign,
            'METHOD'                         => "SetExpressCheckout",
            'VERSION'                        => 86,
            'PAYMENTREQUEST_0_PAYMENTACTION' => "AUTHORIZATION",
            'PAYMENTREQUEST_0_AMT'           => $amount,
            'PAYMENTREQUEST_0_CURRENCYCODE'  => $unit,
            'L_BILLINGTYPE0'                 => "MerchantInitiatedBilling",
            'L_BILLINGAGREEMENTDESCRIPTION0' => "ClubUsage",
            'cancelUrl'                      => $cancelUrl,
            'returnUrl'                      => $returnUrl,
        ];

        $res = $this->doCurl($this->agragreementUrl, http_build_query($data));
        // 解析参数
        parse_str($res, $output);

        if (strtolower($output['ACK']) == "success" || strtolower($output['ACK']) == "successwithwarning") {
            $this->redirect($this->excuteUrl.urlencode($output['TOKEN']));
        }else{
            return $output;
        }
    }

    // 同意协议
    public function agreement($token, $playId, $amount)
    {

        // 请求数据
        $data = [
            'USER'      => $this->merchantName,
            'PWD'       => $this->merchantPass,
            'SIGNATURE' => $this->merchantSign,
            'METHOD'    => "CreateBillingAgreement",
            'VERSION'   => 86,
            'TOKEN'     => $token,
        ];

        $res = $this->doCurl($this->agragreementUrl, http_build_query($data));
        // 解析参数
        parse_str($res, $output);

        // 捕获当前购买
        if (strtolower($output['ACK']) == "success" || strtolower($output['ACK']) == "successwithwarning") {
            // 请求数据
            $data = [
                'USER'                           => $this->merchantName,
                'PWD'                            => $this->merchantPass,
                'SIGNATURE'                      => $this->merchantSign,
                'METHOD'                         => "DoExpressCheckoutPayment",
                'VERSION'                        => 86,
                'TOKEN'                          => $token,
                'PAYERID'                        => $playId,
                'PAYMENTREQUEST_0_AMT'           => $amount,
                'PAYMENTREQUEST_0_PAYMENTACTION' => "sale",
            ];

            $res = $this->doCurl($this->agragreementUrl, http_build_query($data));
            // 解析参数
            parse_str($res, $output);

            if (strtolower($output['ACK']) == "success" || strtolower($output['ACK']) == "successwithwarning") {
                return ['referenceid' => $output['BILLINGAGREEMENTID']];
            }else{
                return $output;
            }

        }else{
            return $output;
        }
    }

    // 未来付款
    public function futurePay($billId)
    {

        // 请求数据
        $data = [
            'USER'          => $this->merchantName,
            'PWD'           => $this->merchantPass,
            'SIGNATURE'     => $this->merchantSign,
            'METHOD'        => "DoReferenceTransaction",
            'VERSION'       => 86,
            'AMT'           => 50,
            'CURRENCYCODE'  => "USD",
            'PAYMENTACTION' => "SALE",
            'REFERENCEID'   => $billId,
        ];

        $res = $this->doCurl($this->agragreementUrl, http_build_query($data));
        // 解析参数
        parse_str($res, $output);

        if (strtolower($output['ACK']) == "success" || strtolower($output['ACK']) == "successwithwarning") {
            return "success";
        }else{
            return $output;
        }
    }

    // 请求接口
    public function doCurl($url, $post = false){
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        //避免https 的ssl验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if($post){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
        }

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }
}
