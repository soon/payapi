<?php
namespace app\api\service;

use think\Controller;
use think\facade\Env;
use PayPalCheckoutSdk\Core\PayPalHttpClient; 
use PayPalCheckoutSdk\Core\SandboxEnvironment; 
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest; 
use PayPalCheckoutSdk\Orders\OrdersGetRequest;

/**
 * Paypal 智能支付按钮
 * @author sam
 * Date 2019/07/12
 */
class Paypalsmart extends Controller
{

    private $client   = '';

    public function __construct()
    {
        parent::__construct();

        $config  = config("logic.paypal_app");
        $environment  = new SandboxEnvironment($config['app_clientid'], $config['app_secret']);
        $this->client = new PayPalHttpClient($environment);
    }

    public function createOrder($data, $debug = false)
    {
        $request = new OrdersCreateRequest();
        $request->prefer('return=representation');
        $request->body = $data;

        $response = $this->client->execute($request);
        if ($debug)
        {
            print "Status Code: {$response->statusCode}\n";
            print "Status: {$response->result->status}\n";
            print "Order ID: {$response->result->id}\n";
            print "Intent: {$response->result->intent}\n";
            print "Links:\n";
            foreach($response->result->links as $link)
            {
                print "\t{$link->rel}: {$link->href}\tCall Type: {$link->method}\n";
            }
        }

        return $response;
    }


    public function captureOrder($orderId, $debug = false)
    {
        $request = new OrdersCaptureRequest($orderId);

        $response = $this->client->execute($request);

        if ($debug)
        {
            print "Status Code: {$response->statusCode}\n";
            print "Status: {$response->result->status}\n";
            print "Order ID: {$response->result->id}\n";
            print "Links:\n";
            foreach($response->result->links as $link)
            {
                print "\t{$link->rel}: {$link->href}\tCall Type: {$link->method}\n";
            }
            print "Capture Ids:\n";

            foreach($response->result->purchase_units as $purchase_unit)
            {
                foreach($purchase_unit->payments->captures as $capture)
                {   
                    print "\t{$capture->id}";
                }
            }
        }
        return $response;
    }

    public function getOrder($orderId)
    {
        $response    = $this->client->execute(new OrdersGetRequest($orderId));

        return $response;

        /**
         *Enable the following line to print complete response as JSON.
         */
        //print json_encode($response->result);
        print "Status Code: {$response->statusCode}\n";
        print "Status: {$response->result->status}\n";
        print "Order ID: {$response->result->id}\n";
        print "Intent: {$response->result->intent}\n";
        print "Links:\n";
        foreach($response->result->links as $link)
        {
        print "\t{$link->rel}: {$link->href}\tCall Type: {$link->method}\n";
        }
        // 4. Save the transaction in your database. Implement logic to save transaction to your database for future reference.
        print "Gross Amount: {$response->result->purchase_units[0]->amount->currency_code} {$response->result->purchase_units[0]->amount->value}\n";

        // To print the whole response body, uncomment the following line
        // echo json_encode($response->result, JSON_PRETTY_PRINT);
        return json($response);
    }

}
