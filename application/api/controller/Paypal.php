<?php
namespace app\api\controller;

use think\Controller;
use app\api\logic\StripeLogic;
use app\api\logic\AgreeLogic;
use app\api\logic\SmartLogic;

/**
 * paypal 支付入口
 * @author sam
 * Date 2019/07/17
 */
class Paypal extends Controller
{
    // 创建Paypal订单
    public function createOrder()
    {
        $tradeId = input("param.trade_id/d", '');

        $logic = new SmartLogic();
        $response = $logic->createOrder($tradeId);

        return json($response);
    }

    // 支付订单
    public function payOrder()
    {
        $tradeId = input("param.trade_id/d", '');
        $orderId = input("param.orderID/s");

        $logic = new SmartLogic();
        $response = $logic->payOrder($tradeId, $orderId);

        return json($response);
    }

    // 验证订单
    public function verfyOrder()
    {
        $orderId = input("param.orderID/s");
        
        $logic = new SmartLogic();
        $response = $logic->verfyOrder($orderId);

        return json($response);
    }

    // 协议支付
    public function agreement()
    {
        $logic = new AgreeLogic();

        // 获取请求参数
        $tradeId = input("param.trade_id/d", 0);
        if (empty($tradeId)) return $logic->returnMsg(103);

        // 获取请求参数
        $info = $logic->getInfo($tradeId);
        if (empty($info)) return $logic->returnMsg(105);

        // 跳转地址
        $info['success'] = url('authSuccess', ['trade_id' => $tradeId], true, true);
        $info['cancel']  = url('authFailed', '', true, true);

        $res = $logic->payment($info);
        // return $res;
        halt($res);
    }

    // 授权成功
    public function authSuccess()
    {
        $tradeId = input("param.trade_id/d", '');
        $token   = input("param.token/s", '');
        $playId  = input("param.PayerID/s", '');

        // 执行付款
        $logic = new AgreeLogic();
        $data  = $logic->agreement($tradeId, $token, $playId);

        // return $data;
        halt($data);
    }

    // 取消授权
    public function authFailed()
    {
        $logic = new AgreeLogic();
        // return $logic->returnMsg(106);
        halt($logic->returnMsg(106));
    }

}
