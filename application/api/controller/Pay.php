<?php
namespace app\api\controller;

use think\Controller;
use app\api\logic\StripeLogic;
use app\api\logic\AgreeLogic;
use app\api\logic\SmartLogic;

/**
 * 接口支付入口
 * @author sam
 * Date 2019/07/24
 */
class Pay extends Controller
{

    public function index()
    {
        if (!request()->isPost()) {
            // return $this->returnMsg(102);
        }

        // 获取请求参数
        $data = input("post.");
        
        // 模拟数据
        $data = [
            'goods_title'  => 'Test goods',
            'goods_desc'   => 'This is test payment',
            'goods_amount' => 6.66,
            'goods_unit'   => 'USD',
            'user_id'      => 12,
            'type'         => input("get.type/d", 1),
        ];

        // 参数过滤
        $data = $this->format($data);
        if (is_object($data)) return $data;

        $type = $data['type'];
        if ($type == 1) {// paypal协议支付

            // 保存数据库
            $logic = new AgreeLogic();
            $tradeId = $logic->saveRequest($data);
            if (empty($tradeId)) return $this->returnMsg(106);

            $list = [
                'url'      => urlencode(url('agreement', [], false, true)),
                'trade_id' => $tradeId,
            ];
            // 创建成功
            return $this->returnMsg(200, 1, '', $list);

        }elseif ($type == 2) {// paypal常规支付

            // 存库
            $logic = new SmartLogic();
            $tradeId   = $logic->saveRequest($data);
            if (empty($tradeId)) return $this->returnMsg(106);

            $list = [
                'url'      => url('smart', [], false, true),
                'trade_id' => $tradeId,
            ];
            // 成功
            return $this->returnMsg(200, 1, '', $list);

        }elseif ($type == 3) {// stripe协议支付

            $logic = new StripeLogic();

            // 保存请求信息
            $tradeId = $logic->saveRequest($data);
            if (empty($tradeId)) return $logic->returnMsg(106);

            $res = $logic->payAgree($tradeId);
            // 协议支付
            if (!empty($res)) {
                // return $data;
                halt($res);
            }else{

                $list = [
                    'url'      => url('stripe', [], false, true),
                    'trade_id' => $tradeId,
                ];
                // 成功
                return $this->returnMsg(200, 1, '', $list);
            }
        }
    }

    // paypal 协议支付
    public function agreement()
    {
        $tradeId = input("param.trade_id/d", '');
        if (empty($tradeId)) {
            echo "【trade_id】 不存在！";exit;
        }

        $logic = new AgreeLogic();
        $data  = $logic->getInfo($tradeId);

        $info = [
            'goods_title'  => $data['goods_title'],
            'goods_desc'   => $data['goods_desc'],
            'goods_amount' => $data['goods_amount'],
            'goods_unit'   => $data['goods_unit'],
        ];
        $this->assign("info", $info);
        $this->assign("trade_id", $tradeId);
        return $this->fetch();
    }

    // paypal 常规支付
    public function smart()
    {
        $tradeId = input("param.trade_id/d", '');
        if (empty($tradeId)) {
            echo "【trade_id】 不存在！";exit;
        }

        $clientId = config("logic.paypal_app.app_clientid");
        $this->assign("clientId", $clientId);

        $logic = new SmartLogic();
        $data  = $logic->getInfo($tradeId);

        $info = [
            'goods_title'  => $data['goods_title'],
            'goods_desc'   => $data['goods_desc'],
            'goods_amount' => $data['goods_amount'],
            'goods_unit'   => $data['goods_unit'],
        ];
        $this->assign("info", $info);
        $this->assign("trade_id", $tradeId);
        return $this->fetch();
    }

    // stripe 协议支付
    public function stripe()
    {
        $tradeId = input("param.trade_id/d", '');
        if (empty($tradeId)) {
            echo "【trade_id】 不存在！";exit;
        }

        $publish = config("logic.stripe_config.publish_key");
        $this->assign("publish", $publish);
        $this->assign("trade_id", $tradeId);
        return $this->fetch();
    }

    // Stripe
    public function payStripe()
    {
        $logic = new StripeLogic();
        if (!request()->isPost()) {
            return $logic->returnMsg(102);
        }

        // 获取请求参数
        $data = input("post.");
        // 支付
        $res  = $logic->payment($data);
        // return $res;
        halt($res);    
    }

    // 参数过滤
    public function format($data)
    {
        // 用户标识
        if (empty($data['user_id'])) return $this->returnMsg(104, 0, 'user_id');
        // 商品标题
        if (empty($data['goods_title'])) return $this->returnMsg(104, 0, 'goods_title');
        // 商品描述
        if (empty($data['goods_desc'])) return $this->returnMsg(104, 0, 'goods_desc');
        // 支付金额
        if (empty($data['goods_amount'])) return $this->returnMsg(104, 0, 'goods_amount');
        if (!is_numeric($data['goods_amount'])) return $this->returnMsg(105, 0, 'goods_amount');
        if ($data['goods_amount'] <= 0) return $this->returnMsg(105, 0, 'goods_amount');
        // 货币单位
        if (empty($data['goods_unit'])) return $this->returnMsg(104, 0, 'goods_unit');
        // 支付方式
        if (empty($data['type'])) return $this->returnMsg(104, 0, 'type');
        if (!is_int($data['type'])) return $this->returnMsg(105, 0, 'type');
        if ($data['type'] < 1 || $data['type'] > 3) return $this->returnMsg(105, 0, 'type');

        return $data;
    }

    /**
     * 接口输出
     *
     * @param      intval   $code     错误代码
     * @param      boolean  $status   请求结果
     * @param      string   $param    参数
     * @param      array    $data     响应消息
     *
     * @return     array    ( response )
     */
    public function returnMsg($code, $status = false, $param = '', $data = [])
    {
        $msg  = config("logic.return_msg");
        $data = [
            'result'   => empty($status)? "fail": "success",
            'code'     => $code,
            'message'  => empty($param)? $msg[$code]: $msg[$code]." - ".$param,
            'param'    => $param,
            'data'     => $data,
        ];

        return json($data);
    }
}
