支付接口文档（Paypal，Stripe）
==========================

开发环境

- PHP version 7.2
- Mysql version 5.7
- PHP framework Thinkphp 5.1
- IDE PHPStudy+SublimeText3

本接口主要集成了三种支付方式：

* Paypal协议支付
* Paypal常规支付
* Stripe协议支付

## 接口地址

**www.example.com/api/pay/index**

## 调用流程

- 带参数请求接口，失败会返回错误信息，成功返回支付页面地址与数据ID

示例
```php
	// 请求参数
	$data = [
        'goods_title'   => '',
        'goods_desc'    => '',
        'goods_amount'  => ,
        'goods_unit'    => '',
        'type'     		=> 1,
        'user_id'       => '',

	];
	// post请求接口  curl函数请自行封装
	$res = curl("api_url", $data);
```
响应
```json
	{
		"result":"success",
		"code":200,
		"message":"创建成功",
		"param":"",
		"data":
		{"url":"",
			"trade_id":""
		}
	}
```

> 错误代码 

```php
	[
		102 => '请求方式错误',
		103 => '请选择支付方式',
		104 => '参数缺失',
		105 => '参数格式错误',
		106 => '支付创建失败',
		199 => '服务器错误',
		200 => '创建成功',
	]
```

- 访问页面地址，确认支付信息，进行支付

> 请将返回的url地址urlcode解码后再访问

示例
```html
	get www.example.com?trade_id=1
```

- 获取返回信息，完成支付

返回数据(示例)
```php
	[
		// 响应状态 success/fail
	    'result'   => "success",
	    // 响应代码
	    'code'     => '',
	    // 文字信息
	    'message'  => '',
	    // 支付接口错误代码
	    'api_code' => '',
	    // 支付接口错误信息
	    'api_msg'  => '',
	    // 数据返回
	    'data'     => '',
	]
```

## 接口调用

> 调用接口需要配置好三个接口的秘钥参数，需要开发人员操作

### 参数说明
```php
    [
        // 商品标题
        'goods_title'   => '',
        // 商品描述
        'goods_desc'    => '',
        // 支付价格
        'goods_amount'  => ,
        // 货币单位
        'goods_unit'    => '',
        // 支付方式 1：paypal协议支付 2：paypal常规支付 3：stripe支付
        'type'     => 1,
        // 业务方用户标识ID
        'user_id'         => "",
    ];
```

## Paypal协议支付

### 简介

*   A reference transaction is a financial transaction from which subsequent transactions can be derived. For example, When a buyer makes a purchase on your site, PayPal generates a transaction ID, called a reference transaction ID. Later, you can use the reference ID to initiate another transaction

### 接口调用

> 调用接口时返回支付确认页面，用户确认后调用支付接口执行支付
> 若用户为第一次支付，会跳转到paypal授权页面进行授权支付
> 若已经获取到授权，会直接跳转到paypal支付确认页面

#### 参数说明
```php
    [
        // 商品标题
        'goods_title'   => '',
        // 商品描述
        'goods_desc'    => '',
        // 支付价格
        'goods_amount'  => ,
        // 货币单位
        'goods_unit'    => '',
        // 支付方式 1：paypal协议支付 2：paypal常规支付 3：stripe支付
        'type'          => 1,
        // 业务方用户标识ID
        'user_id'       => "",
    ];
```
#### 接口响应

> 信息返回格式

```php
	[
		// 响应状态 success/fail
	    'result'   => "success",
	    // 响应代码
	    'code'     => '',
	    // 文字信息
	    'message'  => '',
	    // 支付接口错误代码
	    'api_code' => '',
	    // 支付接口错误信息
	    'api_msg'  => '',
	    // 数据返回
	    'data'     => '',
	]
```

> 错误信息代码

```php
	[
		102 => '请求方式错误',
		103 => '请求参数缺失',
		104 => '支付参数缺失',
		105 => '数据储存失败',
		106 => '授权取消',
		107 => '授权失败',
		108 => '支付失败',
		109 => '授权支付失败',
		199 => '服务器连接失败',
		200 => '支付成功',
	]

```


## Paypal常规支付

### 简介

*   PayPal Checkout with Smart Payment Buttons gives your buyers a simplified and secure checkout experience. PayPal intelligently presents the most relevant payment types to your shoppers, automatically, making it easier for them to complete their purchase using methods like Pay with Venmo, PayPal Credit, credit card payments, iDEAL, Bancontact, Sofort, and other payment types.

### 接口调用

> 调用接口时返回支付确认页面，用户确认后调用支付接口执行支付
> 用户确认后新建浏览器窗口，需要用户登录paypal账号进行登录确认支付
> 支付成功后页面会有消息提示支付成功

#### 请求参数
```php
	[
		// 应用ID
	    'app_clientid' => '',
	    // 应用秘钥
	    'app_secret'   => '',
	    // 商品标题
	    'goods_title'  => '',
	    // 商品描述
	    'goods_desc'   => '',
	    // 交易金额
	    'goods_amount' => 6.66,
	    // 货币代码
	    'goods_unit'   => '',
        // 支付方式 1：paypal协议支付 2：paypal常规支付 3：stripe支付
        'type'     => 2,
        // 业务方用户标识ID
        'user_id'         => "",
	]
```
#### 响应

> 该接口在客户端发起支付，要求客户登录并完成支付，因此，响应信息在页面提示


## Stripe协议支付

### 简介

*   Once you’ve securely collected and tokenized your customer’s credit card using the legacy version of Checkout or Elements, you can charge the card immediately or save it for later. Unlike tokenization, which occurs in the browser, charge attempts are made from your server, normally using one of our client libraries. If you haven’t already, install the library for your favorite language now

### 接口调用

> 若用户第一次使用该支付方式，会返回支付信息页面，需要用户填写银行卡等信息，提交后再执行支付
> 若用户已经授权使用stripe支付，接口会直接执行支付操作，返回支付结果

#### 请求参数
```php
	[
        // 商品标题
        'goods_title'  => '',
        // 商品描述
        'goods_desc'   => '',
        // 商品金额
        'goods_amount' => 6.66,
        // 货币代码
        'goods_unit'   => 'USD',
        // 支付方式 1：paypal协议支付 2：paypal常规支付 3：stripe支付
        'type'     => 3,
        // 业务方用户标识ID
        'user_id'         => "",
    ]
```
#### 接口响应

> 响应格式

```php
	[
		// 响应状态 success/fail
	    'result'   => "success",
	    // 响应代码
	    'code'     => '',
	    // 文字信息
	    'message'  => '',
	    // 支付接口错误代码
	    'api_code' => '',
	    // 支付接口错误信息
	    'api_msg'  => '',
	    // 数据返回
	    'data'     => '',
	]
```

> 错误代码 

```php
	[
		102 => '请求方式错误',
		103 => '请求参数缺失',
		104 => '数据储存失败',
		105 => '银行卡注册失败',
		106 => '客户注册失败',
		107 => '支付发起失败',
		199 => '服务器连接失败',
		200 => '支付成功',
	]
```

> 参考链接

- [Smart Payment Buttons Overview](https://developer.paypal.com/docs/checkout/)
- [Reference transactions](https://developer.paypal.com/docs/classic/express-checkout/integration-guide/ECReferenceTxns/#)
- [Stripe Creating Charges](https://stripe.com/docs/charges)
- [Thinkphp5.1](https://www.kancloud.cn/manual/thinkphp5_1/353946)


